﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectionInterface : MonoBehaviour {
    ////////////////////////////////Data that needs to be explicitly set by the user of this modular code/////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //public GameObject hmd;   //Assign to the one and only HMD camera gameobject (its position and orientation have to be driven by the real time motion tracking data)
    //public GameObject VIVESystem;  //parent object of the HMD and controllers, used to add offset to the motion tracking system

    //public GameObject redirectionZone;  //the gameObject that contains the collider of the redirection zone (the "elbow" area, where rotation gain should be added)
    //public GameObject startingZone; //the gameObject that contains the collider of zone before entering the redirection zone
    //public GameObject endingZone; //the gameObject that contains the collider of zone after exiting the redirection zone

    public Light[] startingZoneLights;  //the lights in the starting zone, which should be dimmed after the user enters the redirection zone
    public Light[] redirectionZoneLights;  //the lights in the redirection zone
    public Light[] endingZoneLights;    //the lights in the ending zone, which starts initially dark and should be lightened up after user pushes the button

    public float basedLineRotationAngle = 0.0f;   //constant rotation gain. Based on my experience, having a constant rotation gain breaks the illusion so it's default to 0
                                                  //The reason it's here is that there was a constant rotation gain in the original RDW paper
    //public GameObject button_group;
    public GameObject button;     //The button, it should contain the collider to detect button activation
                                  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                  // Use this for initialization
    public GameObject redirection_pivot;
    public bool clockwiseRot;
    void Start () {
        if (button != null)
        {
            button.SetActive(false);
        } else
        {
            Debug.LogWarning("A redirection interface does not have a reference to its button. Is this intentional?");
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

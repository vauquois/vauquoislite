﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class end_zone_behavior : MonoBehaviour
{
    public int area_ID;
    public ManageRedirection manageRedirection;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "MainCamera" || col.gameObject.tag == "HeadCollider") {
			//redirectionManager.gameObject.GetComponent<ManageRedirection>().buttonActivation = true;
			//rend.material.SetColor("_Color", Color.green);
			manageRedirection.enterExitZone (area_ID);
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button_click : MonoBehaviour {
    public int area_ID;
    public ManageRedirection manageRedirection;

    private Renderer rend;
    // Use this for initialization
    void Start () {
        //rend = transform.gameObject.GetComponent<Renderer>();
        //rend.material.SetColor("_Color", Color.yellow);


    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.name == "controller_collision")
		if(other.gameObject.tag == "Controller")
        {
			Debug.Log ("controller collision with button");
            //redirectionManager.gameObject.GetComponent<ManageRedirection>().buttonActivation = true;
            //rend.material.SetColor("_Color", Color.green);
            manageRedirection.buttonPressed(area_ID);

            GetComponent<Animator>().Play("Take001");

        }
    }
}

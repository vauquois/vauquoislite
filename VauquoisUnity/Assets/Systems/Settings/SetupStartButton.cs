﻿using UnityEngine;
using UnityEngine.UI;
/**
* Controls the setup and start button
* 
*/
[RequireComponent(typeof(Button))]
public class SetupStartButton : MonoBehaviour
{
    [SerializeField] private ChangeScene changeScene;
    [SerializeField] private Sprite StartImage;
    [SerializeField] private Sprite SetupImage;
    private Button button;
    void Start()
    {
        //UpdateView();

        button = GetComponent<Button>();
        button.onClick.AddListener(() =>
        {
            if (VauquoisSettings.hasCompletedFirstCalibration)
            {
                if (VauquoisSettings.skipOpeningScene)
                {
                    changeScene.LoadTunnelScene(); // They want to skip opening and they've calibrated before
                }
                else
                {
                    changeScene.LoadOpeningScene(); // They dont want to skip opening and they've calibrated before
                }
            }
            else
            {
                changeScene.LoadTunnelScene(); // They need to goto the tunnel scene to calibrate
            }
        });
    }

    private void UpdateView()
    {
        GetComponentInChildren<Image>().sprite = (VauquoisSettings.hasCompletedFirstCalibration) ? StartImage : SetupImage;
    }
}

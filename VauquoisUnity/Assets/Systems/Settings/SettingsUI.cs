﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUI : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown lanternTypeDropdown;

    void Start()
    {
        // Update our view first
        UpdateUIToMatchVauquoisSettings();

        // Then start registering change events
        lanternTypeDropdown.onValueChanged.AddListener(delegate {
            DropdownValueChanged(lanternTypeDropdown);
        });
    }

    void OnDestroy()
    {
        lanternTypeDropdown.onValueChanged.RemoveAllListeners(); //TODO: Maybe this isnt the best idea, but how else do I not leak memory?
    }

    void DropdownValueChanged(TMP_Dropdown change)
    {
        if (Enum.IsDefined(typeof(AdaptiveLanternController.LanternType), change.value))
        {
            VauquoisSettings.lanternType = (AdaptiveLanternController.LanternType) change.value;
        }
        UpdateUIToMatchVauquoisSettings();
        SaveVauquoisSettings();
    }

    public void SaveVauquoisSettings()
    {
        VauquoisSettings.Save();
    }

    private void UpdateUIToMatchVauquoisSettings()
    {
        lanternTypeDropdown.value = (int) VauquoisSettings.lanternType;
    }
}

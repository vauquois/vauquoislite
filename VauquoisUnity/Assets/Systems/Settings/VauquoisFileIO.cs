﻿using System;
using System.IO;
using UnityEngine;

public static class VauquoisFileIO
{
    public static string StoragePath => Application.persistentDataPath;

    public static bool FileExists(string fileName)
    {
        string fileWithFullPath = FileWithFullPath(fileName);
        return File.Exists(fileWithFullPath);
    }

    // Write a string to a file
    public static void WriteToFile(string fileName, string content)
    {
        string fileWithFullPath = FileWithFullPath(fileName);
        File.WriteAllText(fileWithFullPath, content);
    }

    // Read a string from a file
    public static string ReadFromFile(string fileName)
    {
        string fileWithFullPath = FileWithFullPath(fileName);

        string contentJSON = "";
        try
        {
            contentJSON = File.ReadAllText(fileWithFullPath);
        }
        catch (Exception e) //TODO: more error checking
        {
            Debug.LogError($"<b>[VauquoisFileIO]</b> There was an issue loading {fileName} from disk.");
            return null; // TODO: Is this a good thing to do?
        }

        return contentJSON;
    }

    private static string FileWithFullPath(string fileName)
    {
        if (Path.GetFileName(fileName) != fileName)
        {
            throw new Exception("<b>[VauquoisFileIO]</b> 'fileName' is invalid.");
        }
        string fileWithFullPath = Path.Combine(StoragePath, fileName);

        return fileWithFullPath;
    }
}

﻿using System;
using UnityEngine;
using static AdaptiveLanternController;

public static class VauquoisSettings
{
    public static LanternType lanternType;
    public static bool hasCompletedFirstCalibration;
    public static bool skipOpeningScene;
    public static string info;

    private static string settingsFileName = "VauquoisSettings.json";

    private static VauquoisSettingsObject defaultSettings = new VauquoisSettingsObject(LanternType.Controller, false, false, "default");

    static VauquoisSettings()
    {
        // No settings yet
        if (!VauquoisFileIO.FileExists(settingsFileName))
        {
            Debug.LogWarning($"<b>[VauquoisSettings]</b> Could not find {settingsFileName}. No settings available. Default settings will be used.");
            ApplySettingsToSelf(defaultSettings);
            Save();
        }

        // We have settings now tso try to load them, using default settings on a failed load
        ReadSettingsFromFileOrUseDefault();
    }

    private static void ReadSettingsFromFileOrUseDefault()
    {
        // Extract string from file and settings object from string
        // Apply default settings if there is a file IO error or a json error
        string settingsObectJSON = VauquoisFileIO.ReadFromFile(settingsFileName); // TODO: This could be null
        VauquoisSettingsObject settingsObject = JsonUtility.FromJson<VauquoisSettingsObject>(settingsObectJSON); // Handled here ^
        if (settingsObject == null)
        {
            Debug.LogError($"<b>[VauquoisSettings]</b> There was an issue loading {settingsFileName} from disk. Default settings will be used.");
            ApplySettingsToSelf(defaultSettings);
            return;
        }
        ApplySettingsToSelf(settingsObject);
    }

    private static void ApplySettingsToSelf(VauquoisSettingsObject settingsObject)
    {
        lanternType = settingsObject.lanternType;
        hasCompletedFirstCalibration = settingsObject.hasCompletedFirstCalibration;
        skipOpeningScene = settingsObject.skipOpeningScene;
        info = settingsObject.info;
    }

    public static void Save()
    {
        VauquoisSettingsObject settingsObject = new VauquoisSettingsObject(lanternType, hasCompletedFirstCalibration, skipOpeningScene, info);
        string settingsObectJSON = JsonUtility.ToJson(settingsObject);
        VauquoisFileIO.WriteToFile(settingsFileName, settingsObectJSON);
        Debug.Log($"<b>[VauquoisSettings]</b> Current settings saved to {settingsFileName}.");
    }

    [Serializable]
    private class VauquoisSettingsObject
    {
        public string info;
        public LanternType lanternType;
        public bool hasCompletedFirstCalibration;
        public bool skipOpeningScene;

        public VauquoisSettingsObject(LanternType lanternType, bool hasCompletedFirstCalibration, bool skipOpeningScene, string info = "no info")
        {
            this.lanternType = lanternType;
            this.hasCompletedFirstCalibration = hasCompletedFirstCalibration;
            this.skipOpeningScene = skipOpeningScene;
            this.info = info;
        }
    }
}

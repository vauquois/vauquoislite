﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
//using Valve.VR;

public class EndOfExperiencePhotosphere : MonoBehaviour
{
    [Tooltip("The root of the tunnel. This object will be disabled when the experience ends.")]
    [SerializeField]
    private GameObject Tunnel;

    [Tooltip("The root of the lantern. This object will be disabled when the experience ends.")]
    [SerializeField]
    private GameObject Lantern;

    [Tooltip("The 360 photo. This object will be instantiated at the position of the player when the experience ends.")]
    [SerializeField]
    private GameObject PhotospherePrefab;

    [Tooltip("The position of the players head in the scene.")]
    [SerializeField]
    private Transform PlayerPos;

    [Space(20)]

    // Fired after the player is fully faded into the photosphere
    public UnityEvent OnFinishTransition;

    public void Activate()
    {
        StartCoroutine(ActivateRoutine());
    }

    private IEnumerator ActivateRoutine()
    {
        //SteamVR_Fade.Start(Color.black, 3);
        yield return new WaitForSeconds(3);
        TransitionToPhotosphere();
        //SteamVR_Fade.Start(Color.clear, 3);
        yield return new WaitForSeconds(3);
        OnFinishTransition?.Invoke();

        void TransitionToPhotosphere()
        {
            Tunnel.SetActive(false);
            Lantern.SetActive(false);
            Instantiate(PhotospherePrefab, PlayerPos.position, Quaternion.identity);
        }
    }


}

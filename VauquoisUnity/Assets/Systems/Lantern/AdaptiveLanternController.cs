﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Valve.VR;

public class AdaptiveLanternController : MonoBehaviour
{
    // ---------------------------------------------

    public enum LanternType { Controller, Tracker };
    public LanternType lanternType;

    public Transform origin;

    // ---------------------------------------------
    [Space(10)]

    public List<LanternConfiguration> configurations;

    [Serializable] //TODO: Make a way to save these in editor :)
    public class LanternConfiguration
    {
        public LanternType type;
        public Vector3 localPosition;
        public Vector3 localRotation; // Note this is Euler
    }

    // ---------------------------------------------

    // Start is called before the first frame update
    void Start()
    {
        lanternType = VauquoisSettings.lanternType;
        FindSuitableTrackedIndex(); //TODO: Do this on any new poses?
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ApplyLanternConfiguration(LanternConfiguration config)
    {
        // Set position
        origin.localPosition = config.localPosition;
        origin.localRotation = Quaternion.Euler(config.localRotation);

        // Set rigidbodystate
    }

    private void FindSuitableTrackedIndex()
    {
        /*
        SteamVR_TrackedObject tr = GetComponent<SteamVR_TrackedObject>();

        for (uint i = 0; i < OpenVR.k_unMaxTrackedDeviceCount; i++)
        {
            ETrackedDeviceClass deviceClass = OpenVR.System.GetTrackedDeviceClass(i);
            if (deviceClass == ETrackedDeviceClass.GenericTracker && lanternType == LanternType.Tracker)
            {
                Debug.Log("Found Tracker");
                OnFoundUsableIndex((int)i, LanternType.Tracker);
                return;
            }

            if (deviceClass == ETrackedDeviceClass.Controller && lanternType == LanternType.Controller)
            {
                Debug.Log("Found Controller");
                OnFoundUsableIndex((int) i, LanternType.Controller);
                return;
            }
        }

        OnDidNotFindUsableIndex();

        void OnDidNotFindUsableIndex()
        {
            tr.gameObject.SetActive(false);
        }

        void OnFoundUsableIndex(int i, LanternType t)
        {
            Debug.Log("OnFoundUsableIndex");
            tr.SetDeviceIndex(i);
            ApplyLanternConfiguration(configurations.Find(c => c.type == t));
        }
        */
    }
}

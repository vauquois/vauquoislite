﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanternHintController : MonoBehaviour {

    public GameObject hintCanvas;
    public GameObject lantern;
    public Transform HintLocation;
    public float hintFadeThresholdDist = 3;

    public Vector3 initialPosition;
    private bool movedFlag;
    private bool stableFlag;

	// Use this for initialization
	void Start () {
        showHint();
        StartCoroutine(stabalizeInititialPos());
        
        // Put the hint canvas right above the lantern
        hintCanvas.transform.SetParent(HintLocation);
        hintCanvas.transform.localPosition = Vector3.zero;
        hintCanvas.transform.localRotation = Quaternion.identity;
	}
	
	// Update is called once per frame
	void Update () {

        // Check for movement (bool short circuit to cut on calls)
		if (!movedFlag && stableFlag && hasMoved())
        {
            movedFlag = true;
            hideHint();
        }
	}

    private bool hasMoved()
    {
        if (Vector3.Distance(lantern.transform.position, initialPosition) > hintFadeThresholdDist)
        {
            return true;
        }
        return false;
    }

    private void showHint()
    {
        hintCanvas.SetActive(true);
    }

    private void hideHint()
    {
        hintCanvas.SetActive(false);
    }

    private IEnumerator stabalizeInititialPos()
    {
        yield return new WaitForSeconds(3);

        initialPosition = lantern.transform.position;

        stableFlag = true;
    }
}

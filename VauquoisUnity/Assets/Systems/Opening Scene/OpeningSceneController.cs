﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
//using Valve.VR;
//using static CalibrationBackend;

/**
 * Controls the opening scene
 * 
 * Note: The opening scene root should have objects placed inside it such that the forward (z, blue) 
 * direction is where the player should be looking at the start. 
 * This is the direction that will be aligned with the tunnel calibration direction.
 */
public class OpeningSceneController : MonoBehaviour {

    [Header("Assignments")]
	public AudioSource narrationAudioSource;
	public GameObject tunnelLaunchUICanvas;
    public GameObject reticlePrefab;
    public GameObject FaceUICanvas;
    public Transform openingSceneRoot;

    [Header("Settings")]

    [SerializeField, Range(0f, 10f)]
    public float fadeTime = 4f;

    [Space(20)]

    // Fired after the player is fully faded into the scene
    public UnityEvent OnFinishTransition;

    private bool bAudioStarted = false;
    private bool bTriggered = false;

    private void Start () {
        // Get audio routine started
		StartCoroutine(DelayThenSetAudioToStarted ());

        // Then make sure "Look Here to Start" is disabled
        tunnelLaunchUICanvas.SetActive(false);

        // Then align the scene to the current calibration
        /*
        if (CalibrationBackend.CalibrationPresent())
        {
            Debug.Log($"<b>[{GetType()}]</b> Calibration data found. Opening Scene will be aligned.");

            // There is already a calibration present, load that
            AlignToCurrentTunnelCalibration();
        }
        else
        {
            Debug.LogWarning($"<b>[{GetType()}]</b> No saved calibration data available. This is an issue, the opening scene will not be aligned to tunnel direction.");
        }
        */
    }
	
	private void Update () {
		if (bAudioStarted && !narrationAudioSource.isPlaying && !bTriggered) {
			tunnelLaunchUICanvas.SetActive (true);
            Instantiate(reticlePrefab, FaceUICanvas.transform);
            bTriggered = true;
		}
	}

	private IEnumerator DelayThenSetAudioToStarted(){
        //SteamVR_Fade.Start(Color.black, 0);
        //SteamVR_Fade.Start(Color.clear, fadeTime);
        yield return new WaitForSeconds(fadeTime);
        bAudioStarted = true;
        OnFinishTransition?.Invoke();
    }

    private void AlignToCurrentTunnelCalibration()
    {
        //CalibrationData data = CalibrationBackend.LoadMostRecent();
        //openingSceneRoot.rotation = data.Rotation;
        // TODO: Instead of relying on the note in the header, use front and back anchors to align the opening scene jsut like the tunnel is aligned.
    }
}

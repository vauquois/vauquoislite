﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningSceneImageController : MonoBehaviour {


	public GameObject image_preWarPainting;
	public GameObject image_hillWhiteSkull;
	public GameObject image_landscapeFromSky;
	public GameObject image_map;
	public GameObject image_map02;
	public GameObject image_vauquoisMap;
	public GameObject image_villageWest;

    bool bSequenceTriggered = false;

	void Start(){
		image_preWarPainting.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
		image_hillWhiteSkull.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
		image_landscapeFromSky.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
		image_map.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
		image_map02.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
		image_vauquoisMap.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
		image_villageWest.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
        StartCoroutine(DoImageFading());
	}
		

	private IEnumerator DoImageFading(){
        // Wait before showing anything
		yield return new WaitForSeconds (5f);

		StartCoroutine(FadeInMaterial (image_preWarPainting.GetComponent<Renderer> ().material, 1.5f));
		yield return new WaitForSeconds (4);

        StartCoroutine(FadeInMaterial (image_villageWest.GetComponent<Renderer> ().material, 1.5f));
        yield return new WaitForSeconds(6);

        StartCoroutine(FadeInMaterial(image_vauquoisMap.GetComponent<Renderer>().material, 1.5f));
        yield return new WaitForSeconds(6);

        StartCoroutine(FadeInMaterial(image_map02.GetComponent<Renderer>().material, 1.5f));
        StartCoroutine(FadeOutMaterial(image_villageWest.GetComponent<Renderer>().material, 1.5f));
        yield return new WaitForSeconds(6);

		StartCoroutine(FadeInMaterial (image_map.GetComponent<Renderer> ().material, 1.5f));
        StartCoroutine(FadeOutMaterial(image_vauquoisMap.GetComponent<Renderer>().material, 1.5f));
        yield return new WaitForSeconds(6);

        StartCoroutine(FadeInMaterial (image_landscapeFromSky.GetComponent<Renderer> ().material, 1.5f));
        StartCoroutine(FadeOutMaterial(image_map02.GetComponent<Renderer>().material, 1.5f));

        yield return new WaitForSeconds (6);
        StartCoroutine(FadeOutMaterial(image_map.GetComponent<Renderer>().material, 1.5f));

        StartCoroutine(FadeOutMaterial(image_landscapeFromSky.GetComponent<Renderer>().material, 1.5f));
        yield return new WaitForSeconds(1);

        StartCoroutine(FadeOutMaterial (image_preWarPainting.GetComponent<Renderer> ().material, 1.5f));
        yield return new WaitForSeconds(1);
        StartCoroutine(FadeInMaterial (image_hillWhiteSkull.GetComponent<Renderer> ().material, 1.5f));
        yield return new WaitForSeconds(5);

        yield return new WaitForSeconds(1);
        StartCoroutine(FadeOutMaterial (image_hillWhiteSkull.GetComponent<Renderer> ().material, 5f));
	}

	private IEnumerator FadeInMaterial(Material mat, float time){
		while (mat.color.a < 1){
			mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a + .015f);
			yield return new WaitForSeconds (time / 60f); // divide the time to fade by a guessed at frame rate
		}
	}

	private IEnumerator FadeOutMaterial(Material mat, float time){
		while (mat.color.a > 0){
			mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a - .015f);
			yield return new WaitForSeconds (time / 60);
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

public class GazePercentToFillAmount: MonoBehaviour {
	public OpeningSceneRaycaster openingSceneCameraRaycastController;
    public Image image;
	void Update () {
        image.fillAmount = openingSceneCameraRaycastController.percent;
	}
}

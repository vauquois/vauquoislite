﻿using UnityEngine;
using UnityEngine.SceneManagement;
//using Valve.VR;

public class OpeningSceneRaycaster : MonoBehaviour {

    public ChangeScene changeScene;
    public float timeToHoldGaze = 100f;
    public GameObject objectToCollideWith;

    private float gazeHeldTime = 0f;
    private bool bTriggered = false;

    public float percent => gazeHeldTime / timeToHoldGaze;
	
	void Update () {
        if (bTriggered) return;

		if (gazeHeldTime >= timeToHoldGaze) {
            //SteamVR_Fade.Start(Color.black, 1);
            Invoke("loadTunnelScene", 1);
            bTriggered = true;
			return;
		}

		Ray ray = new Ray (transform.position, transform.forward);
		RaycastHit hit;
        Debug.DrawRay(ray.origin, ray.direction*100);
		if (Physics.Raycast (ray, out hit, 100)) {
            Debug.DrawRay(ray.origin, ray.direction*100, Color.red);
            if (hit.collider.gameObject == objectToCollideWith) {
				gazeHeldTime += .5f;
			} else
            {
			    gazeHeldTime = 0;
            }
		} else
        {
            gazeHeldTime = 0;
        }
    }

    private void loadTunnelScene()
    {
        changeScene.LoadTunnelScene();
    }
}

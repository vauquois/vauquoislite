﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpeningSceneCameraRaycastToChangeScenes : MonoBehaviour {

	public float gazeHeldTime;
	
	// Update is called once per frame
	void Update () {
		if (gazeHeldTime >= 100) {
			SceneManager.LoadScene ("VauquoisTunnelLeap");
			return;
		}
		Ray ray = new Ray (transform.position, transform.forward);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, 100)) {
			if (hit.collider.gameObject.name == "SceneChangeText") {
				Debug.Log ("colliding with text!");
				gazeHeldTime += .5f;
			}
		} else {
			gazeHeldTime = 0;
		}
	}
}

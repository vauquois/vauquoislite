﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[ExecuteInEditMode]
public class SpectatorController : MonoBehaviour {

    public bool bUseMomentum;

    [HideInInspector]
    public float yElevationBoost = 0;

    public float movementSpeed = 0.25f;

    public float mouseSensitivityX = 3F;
    public float mouseSensitivityY = 3F;

    public float mouseSmoothWithMomentum = 10;

    public bool bRunInBackground = false;
    public bool bHideCursor = false;

    float myGlobalRotationX = 0f;
    float myGlobalRotationY = 0F;

    bool bToggleOne = false;

    Rigidbody rb;

    float smoothXAxis;
    float smoothYAxis;

    float minimumY = -180F;
    float maximumY = 180F;

    float rotationY = 0F;

    void Start() {
        myGlobalRotationX = GetComponent<Transform>().eulerAngles.y;
        myGlobalRotationY = GetComponent<Transform>().eulerAngles.x;

        if (bUseMomentum) {
            rb = gameObject.AddComponent<Rigidbody>();
            rb.drag = 2;
            rb.angularDrag = 0;
            rb.mass = 0.05f;
            rb.useGravity = false;
        }
    }

    void Update() {
        Application.runInBackground = bRunInBackground;
        Cursor.visible = !bHideCursor;

        if (Input.GetKeyDown("return")) {
            bToggleOne = !bToggleOne;
        }

        if (bToggleOne) {
            return;
        }

        float moveStrafe = Input.GetAxis("Horizontal");
        float moveForwardBack = Input.GetAxis("Vertical");
        float moveUpDown = Input.GetAxis("Jump");

        Vector3 movement = new Vector3(moveStrafe, moveUpDown, moveForwardBack);
        movement *= movementSpeed;

        if (bUseMomentum) {
            rb.AddRelativeForce(movement);
        }
        else {
            transform.Translate(movement);
        }

        // Method two stores the current rotation from last frame in global variables, adds in the input multiplied by the sensitivity, then assigns that back to the current rotation
        myGlobalRotationX += Input.GetAxis("Mouse X") * mouseSensitivityX;
        myGlobalRotationY += Input.GetAxis("Mouse Y") * mouseSensitivityY;

        if (bUseMomentum) {
            smoothXAxis = Mathf.Lerp(smoothXAxis, Input.GetAxis("Mouse X"), Time.deltaTime * mouseSmoothWithMomentum);
            smoothYAxis = Mathf.Lerp(smoothYAxis, Input.GetAxis("Mouse Y"), Time.deltaTime * mouseSmoothWithMomentum);

            float rotationX = transform.localEulerAngles.y + smoothXAxis * mouseSensitivityX;

            rotationY += smoothYAxis * mouseSensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
        }
        else {
            transform.localEulerAngles = new Vector3(-myGlobalRotationY, myGlobalRotationX, 0);
        }

    }




}



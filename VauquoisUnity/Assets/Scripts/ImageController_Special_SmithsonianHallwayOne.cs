﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageController_Special_SmithsonianHallwayOne : MonoBehaviour {

    public GameObject[] livingQuartersImages = new GameObject[0];
    public GameObject[] faceCarvingImages = new GameObject[0];
    public float imageFadeInTime = 2;
    public float imageFadeOutTime = 2;

    bool bSequenceTriggered = false;

    void Start()
    {
        // make all images invisible
        for (int i = 0; i < livingQuartersImages.Length; i++)
        {
            livingQuartersImages[i].GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);
        }
        for (int i = 0; i < faceCarvingImages.Length; i++)
        {
            faceCarvingImages[i].GetComponent<Renderer>().material.color = new Color(1, 1, 1, 0);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        //Debug.Log ("entered images collider");
        if (!bSequenceTriggered)
        {
            bSequenceTriggered = !bSequenceTriggered;
            StartCoroutine(DoImagesFading());

        }
    }

    private IEnumerator DoImagesFading()
    {
        // fade in living quarters images
        for (int i = 0; i < livingQuartersImages.Length; i++)
        {
            StartCoroutine(FadeInMaterial(livingQuartersImages[i].GetComponent<Renderer>().material, imageFadeInTime));
        }

        // wait 46 seconds for living quarters VO to finish
        yield return new WaitForSeconds(15);

        // fade in face carving images
        for (int i = 0; i < faceCarvingImages.Length; i++)
        {
            StartCoroutine(FadeInMaterial(faceCarvingImages[i].GetComponent<Renderer>().material, imageFadeInTime));
        }

        // wait 31 seconds for face carving VO to finish
        yield return new WaitForSeconds(13);

        // fade out both sets of images
        for (int i = 0; i < livingQuartersImages.Length; i++)
        {
            StartCoroutine(FadeOutMaterial(livingQuartersImages[i].GetComponent<Renderer>().material, imageFadeInTime));
        }
        for (int i = 0; i < livingQuartersImages.Length; i++)
        {
            StartCoroutine(FadeOutMaterial(faceCarvingImages[i].GetComponent<Renderer>().material, imageFadeInTime));
        }
    }


    private IEnumerator FadeInMaterial(Material mat, float time)
    {
        //Debug.Log ("image alpha: " + mat + " ... " + mat.color.a);
        while (mat.color.a < 1)
        {
            //Debug.Log ("increasing alpha by .015");
            mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a + .015f);
            //Debug.Log ("mat changed. new alpha: " + mat + " ... " + mat.color.a);
            yield return new WaitForSeconds(time / 60f); // divide the time to fade by a guessed at frame rate
        }
        //Debug.Log ("done fading in! " + mat + " .... " + mat.color.a);
    }

    private IEnumerator FadeOutMaterial(Material mat, float time)
    {
        //	Debug.Log ("image alpha: " + mat + " ... " + mat.color.a);
        while (mat.color.a > 0)
        {
            //Debug.Log ("decreasing alpha by .015");
            mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a - .015f);
            yield return new WaitForSeconds(time / 60);
        }
    }
}

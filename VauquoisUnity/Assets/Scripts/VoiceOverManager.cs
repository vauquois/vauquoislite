﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoiceOverManager : MonoBehaviour {

    public AudioSource audioSource;
	public Fadeable endOfVRExperienceUITextObject;
    List<AudioClip> queue = new List<AudioClip>();

    void Start() {
        if (audioSource == null) audioSource = GetComponent<AudioSource>();
    }

    void Update() {
        if (queue.Count > 0) {
            if (!audioSource.isPlaying) {
                audioSource.clip = queue[0];
                audioSource.Play();
                queue.RemoveAt(0);
            }
        }

        // TODO: remove this dependence. Possibly extract to UnityEvent and another script for end of experience?
		// after the last audio clip is done playing, trigger the End Of VR Experience UI Text
		if (!audioSource.isPlaying && audioSource.clip.name == "Vauquos Narration VR Clip7 - Coda") {
            endOfVRExperienceUITextObject.Visible = true;
            //Valve.VR.SteamVR_Fade.Start(Color.black, 5);
		}
    }

    public void PlayOrQueueAudio(AudioClip audioClip) {
        if (audioSource == null) audioSource = GetComponent<AudioSource>();
        //Debug.Log ("VoiceOverManager play or queue audio: " + audioClip);
        if (!audioSource.isPlaying) {
            audioSource.clip = audioClip;
            audioSource.Play();
        }
        else {
            queue.Add(audioClip);
        }


		//audioSource.clip = audioClip;
		//audioSource.Play();
    }

	public void PlayAudioOverwriteExisting(AudioClip audioClip){
			

	}
}

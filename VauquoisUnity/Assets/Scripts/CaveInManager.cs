﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaveInManager : MonoBehaviour {

    public GameObject[] rockPrefabs;
    public GameObject spawnPoint;

    public GameObject camera;

    [Tooltip("The lantern to shake. Leave unassigned if you are not using a lantern.")]
    public GameObject lanternObject;
    public ParticleSystem[] particleSystems;

    public float cameraShakeIntensity = 0.01f;
    public float cameraShakeDecay = 0.0001f;

    bool bCaveInEffectsTriggered = false;

    // Use this for initialization
    void Start () {
        //TriggerCaveIn();
        if (lanternObject == null) Debug.LogWarning("CaveIn Manager is missing a reference to the lantern. This is supported, but is this intentional?");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            //TriggerCaveIn();
        }
	}

   public void TriggerCaveIn()
    {
        StartCoroutine(SpawnRocks());

        // shake camera
        if (!bCaveInEffectsTriggered)
        {
            camera.GetComponent<ShakeCamera>().shakeIntensity = cameraShakeIntensity;
            camera.GetComponent<ShakeCamera>().shakeDecay = cameraShakeDecay;
            camera.GetComponent<ShakeCamera>().DoShake();

            if (lanternObject != null) lanternObject.GetComponent<ShakeCamera>().DoShake();

            GetComponent<AudioSource>().Play();

            for (int i = 0; i < particleSystems.Length; i++)
            {
                particleSystems[i].Play();
            }
           // bCaveInEffectsTriggered = true;
        }

    }

    IEnumerator SpawnRocks()
    {
        for (int i = 0; i < 30; i++)
        {
            GameObject rockPrefabToSpawn = rockPrefabs[Random.Range(0, rockPrefabs.Length - 1)];
            Vector3 spawnLocation = spawnPoint.transform.position + new Vector3(Random.Range(-.5f, .5f), Random.Range(0, 2), Random.Range(-.5f, .5f));
            Quaternion spawnRotation = new Quaternion(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1));
            GameObject newRock = Instantiate(rockPrefabToSpawn, spawnLocation, spawnRotation, transform);
            newRock.transform.localScale = new Vector3(Random.Range(.2f, .35f), Random.Range(.2f, .35f), Random.Range(.2f, .35f));
            yield return new WaitForSeconds(0.1f);
        }
    }
}

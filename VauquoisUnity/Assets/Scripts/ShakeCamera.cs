﻿using UnityEngine;
using System.Collections;

public class ShakeCamera : MonoBehaviour {

    public bool shakePosition;
    public bool shakeRotation;

    public float shakeIntensity = 0.5f;
    public float shakeDecay = 0.02f;

    private Vector3 OriginalPos;
    private Quaternion OriginalRot;

    private bool isShakeRunning = false;


    public void DoShake() {
        OriginalPos = transform.localPosition;
        OriginalRot = transform.localRotation;

        StartCoroutine(ProcessShake(OriginalPos, OriginalRot));
    }

    IEnumerator ProcessShake(Vector3 oPos, Quaternion oRot) {
        Debug.Log("process shake");
        if (!isShakeRunning) {
            isShakeRunning = true;
            float currentShakeIntensity = shakeIntensity;

            while (currentShakeIntensity > 0) {
                if (shakePosition) {
                    transform.position = /*OriginalPos*/ transform.position + Random.insideUnitSphere * currentShakeIntensity;
                }
                if (shakeRotation) {
                    transform.rotation = new Quaternion(/*OriginalRot.x*/ transform.rotation.x + Random.Range(-currentShakeIntensity, currentShakeIntensity) * .2f,
                                                        /*OriginalRot.y*/ transform.rotation.y + Random.Range(-currentShakeIntensity, currentShakeIntensity) * .2f,
                                                        /*OriginalRot.z*/ transform.rotation.z + Random.Range(-currentShakeIntensity, currentShakeIntensity) * .2f,
                                                        /*OriginalRot.w*/ transform.rotation.w + Random.Range(-currentShakeIntensity, currentShakeIntensity) * .2f);
                }
                currentShakeIntensity -= shakeDecay;
                yield return null;
            }

            isShakeRunning = false;

            // Return to original pose
            // TODO: Make this smooth
            transform.localPosition = oPos;
            transform.localRotation = oRot;
        }
    }
}
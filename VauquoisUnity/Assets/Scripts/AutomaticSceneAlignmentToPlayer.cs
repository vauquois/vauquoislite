﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticSceneAlignmentToPlayer : MonoBehaviour {

	public Transform playerStartAbsolute;
	public Transform playerStartVirtual;
	public Transform playerCamera;
	public Transform scene;

	public GameObject wireGenerator;

	void Awake(){
		//scene.gameObject.SetActive (false);

	

	}

	// Use this for initialization
	void Start () {
		float yRotDiff = playerStartAbsolute.transform.eulerAngles.y - playerCamera.transform.eulerAngles.y;
		scene.transform.eulerAngles = new Vector3 (scene.transform.rotation.eulerAngles.x, scene.transform.rotation.eulerAngles.y - yRotDiff, scene.transform.rotation.eulerAngles.z);

		Vector3 directPosDiffBetweenAbsoluteAndVirtual = playerStartVirtual.transform.position - playerCamera.transform.position;
		scene.transform.position -= directPosDiffBetweenAbsoluteAndVirtual;

		if (wireGenerator) {
			ConnectPointsWithCylinderMesh[] generators = wireGenerator.GetComponents<ConnectPointsWithCylinderMesh> ();
			foreach (ConnectPointsWithCylinderMesh generator in generators) {
				generator.GenerateWires ();
			}
		}
	}
	

}

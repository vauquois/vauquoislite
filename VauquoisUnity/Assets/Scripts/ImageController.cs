﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageController : MonoBehaviour {

	public GameObject[] images = new GameObject[3];
	public float imageFadeInTime = 2;
	public float waitTime = 20;
	public float imageFadeOutTime = 2;

	bool bSequenceTriggered= false;
		
	void Start(){
		for (int i = 0; i < images.Length; i++) {
			images[i].GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
		}
	}

	void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.name == "VRCamera" || collider.gameObject.tag == "HeadCollider")
        {
            //Debug.Log("entered images collider: " + name);
            if (!bSequenceTriggered)
            {
                bSequenceTriggered = !bSequenceTriggered;
                StartCoroutine(DoImageFading());

            }
        }
    }

	private IEnumerator DoImageFading(){
		for (int i = 0; i < images.Length; i++) {
			StartCoroutine(FadeInMaterial (images[i].GetComponent<Renderer> ().material, imageFadeInTime));
		}
		yield return new WaitForSeconds (waitTime);
		for (int i = 0; i < images.Length; i++) {
			StartCoroutine(FadeOutMaterial (images[i].GetComponent<Renderer> ().material, imageFadeInTime));
		}
	}

	private IEnumerator FadeInMaterial(Material mat, float time){
		//Debug.Log ("image alpha: " + mat + " ... " + mat.color.a);
		while (mat.color.a < 1){
			//Debug.Log ("increasing alpha by .015");
			mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a + .015f);
			//Debug.Log ("mat changed. new alpha: " + mat + " ... " + mat.color.a);
			yield return new WaitForSeconds (time / 60f); // divide the time to fade by a guessed at frame rate
		}
		//Debug.Log ("done fading in! " + mat + " .... " + mat.color.a);
	}

	private IEnumerator FadeOutMaterial(Material mat, float time){
	//	Debug.Log ("image alpha: " + mat + " ... " + mat.color.a);
		while (mat.color.a > 0){
			//Debug.Log ("decreasing alpha by .015");
			mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a - .015f);
			yield return new WaitForSeconds (time / 60);
		}
	}
}

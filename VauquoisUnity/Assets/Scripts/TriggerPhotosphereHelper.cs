﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPhotosphereHelper : MonoBehaviour
{
    [SerializeField] private TriggerPhotosphere photosphere;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider")
        {
            photosphere.activateTrigger();
        }
    }
}

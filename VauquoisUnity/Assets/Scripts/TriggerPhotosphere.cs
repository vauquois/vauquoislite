﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPhotosphere : MonoBehaviour
{
    public VoiceOverManager voiceOverManager;
    public AudioClip audioClip;
    public AudioSource audioSource;
    public GameObject photosphere;
    public GameObject oldTunnel;
    public GameObject newTunnel;
    public float sphereScale = 2.5f;
    public float animTime = 2f;

    bool bTriggered = false;
    bool activatePhoto = false;
    bool deactivatePhoto = false;
    bool audioStarted = false;
    float startTime;

    private void Start()
    {
        startTime = Time.time;
        photosphere.SetActive(false);
    }

    private void Update()
    {
        if (activatePhoto)
        {
            float t = (Time.time - startTime) / animTime;
            photosphere.transform.localScale = Vector3.Slerp(new Vector3(0, 0, 0), new Vector3(sphereScale, sphereScale, sphereScale), t);
            if (t > 1)
            {
                oldTunnel.SetActive(false);
                activatePhoto = false;
            }
        }
        if (audioSource.isPlaying && bTriggered)
        {
            audioStarted = true;
        }
        if (audioStarted && bTriggered && !audioSource.isPlaying)
        {
            newTunnel.SetActive(true);
            deactivatePhoto = true;
            
        }
        if (deactivatePhoto)
        {
            float t = (Time.time - startTime) / animTime;
            photosphere.transform.localScale = Vector3.Slerp(new Vector3(sphereScale, sphereScale, sphereScale), new Vector3(0, 0, 0), t);
            if (t > 1)
            {
                photosphere.SetActive(false);
                deactivatePhoto = false;
                bTriggered = false;
            }
        }
    }

    public void activateTrigger()
    {
        if (!bTriggered)
        {
            // Save trigger so it only activates once
            bTriggered = true;
            // Start audio
            voiceOverManager.PlayOrQueueAudio(audioClip);
            // Switch from tunnel to photosphere
            activatePhoto = true;
            photosphere.SetActive(true);
            startTime = Time.time;
        }
    }
}

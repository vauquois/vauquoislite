﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightUpEndSphereImage : MonoBehaviour {

    public GameObject endSphere;

    private bool bTriggered = false;

	void Start(){
		endSphere.GetComponent<Renderer> ().material.color = new Color (1, 1, 1, 0);
	}

    void OnTriggerEnter(Collider collider) {
        if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider") {
            if (!bTriggered) {
				Debug.Log ("start fading in end sphere");
                StartCoroutine("FadeEndSphere");
                bTriggered = true;
            }
        }
    }

    IEnumerator FadeEndSphere() {
        for (float f = 0f; f <= 1; f += 0.00035f) {
			//Debug.Log ("f: " + f);
            Color c = endSphere.GetComponent<Renderer>().material.color;
            //c.r = f;
			//c.g = f;
			//c.b = f;
			c.a = f;
            endSphere.GetComponent<Renderer>().material.color = c;
            yield return new WaitForSeconds(.01f);
        }
    }
}

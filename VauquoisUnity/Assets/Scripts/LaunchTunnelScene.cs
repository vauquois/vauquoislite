﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LaunchTunnelScene : MonoBehaviour {

	public AudioSource narrationAudioSource;
	public GameObject sceneChangeTextCanvas;
	bool bAudioStarted = false;

	// Use this for initialization
	void Start () {
		StartCoroutine(DelayThenSetAudioToStarted ());
	}
	
	// Update is called once per frame
	void Update () {
		if (bAudioStarted && !narrationAudioSource.isPlaying) {
			sceneChangeTextCanvas.SetActive (true);
		}
	}

	private IEnumerator DelayThenSetAudioToStarted(){
		yield return new WaitForSeconds (1);
		bAudioStarted = true;
	}
}

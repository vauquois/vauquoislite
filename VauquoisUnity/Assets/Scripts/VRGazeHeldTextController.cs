﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRGazeHeldTextController : MonoBehaviour {

	public OpeningSceneCameraRaycastToChangeScenes openingSceneCameraRaycastController;

	void Update () {
		float percentDone = openingSceneCameraRaycastController.gazeHeldTime;
		GetComponent<Text> ().text = percentDone.ToString("F0") + "%";
	}
}

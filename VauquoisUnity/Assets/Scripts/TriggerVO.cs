﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerVO : MonoBehaviour {

    public VoiceOverManager voiceOverManager;
    public AudioClip audioClip;

    bool bTriggered = false;

    void OnTriggerEnter(Collider collider) {
        Debug.Log("trigger vo: " + audioClip);
		if (Time.time > 0.1f) {
			if (collider.gameObject.tag == "MainCamera" || collider.gameObject.tag == "HeadCollider") {
				if (!bTriggered) {
					voiceOverManager.PlayOrQueueAudio (audioClip);
					bTriggered = true;
				}
			}
		}
    }
}

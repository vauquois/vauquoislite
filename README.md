# Vauquois
// TODO: Add Icon

Funded by a 2016-17 ICAT SEAD grant, a transdisciplinary team from Virginia Tech
comprising Thomas Tucker (Creative Technology, School of Visual Arts), 
Todd Ogle (Technology-enhanced Learning and Online Strategies), 
David Hicks (History and Social Science  Education, School of Education), 
DongSoo Choi (Creative Technology, School of Visual Arts), 
and David Cline (Public History, Department of History) created a learning 
environment that is a hands-on, mixed-reality exhibit of the human experience 
in the contested landscape of World War I. In collaboration with French partners 
Celine Beauchamp and Adrien Arles from the archaeology firm Arkimene, 
the team created an immersive experience which leveraged the ICAT Cube, 
a virtual reality walkthrough of a tunnel recreation, and a 360 degree video 
documentary to explore the experiences of French, German and American troops 
and civilians at Vauquois Hill overlooking the French city of Verdun – 
where the shift from streeting fighting to trench warfare to tunnel and 
underground warfare on the Western Front lasting 4 years destroyed the 
village of Vauquois, scarring both the landscape and historical 
consciousness of those who were there.

# Download

You can download Vauquois VR for Windows [here](google.com). You'll need SteamVR and a Vive to use this version. Just unzip, and run!

You can get Vauquois VR for Oculus Quest. Contact jogle@vt.edu for access.

 //TODO: these instructions will change.

# Installation (For Developers)

1. Clone or download this repository. 
2. Install Git LFS. Run ``git lfs install``. Repull or reclone if necessary.
3. Open with Unity. For best results, please use the version found in ``ProjectSettings/ProjectVersion.txt``.

# Documentation

## Branches & Forks

As of 4/8/20, Vauquois VR is broken into three distinct versions

1. The original project, built to run on a desktop with SteamVR and Vive. This is the version on the master branch of [this](https://gitlab.com/historyviz/vauquois) repository.

2. The 'Lite' version, built to run on Oculus Quest. This version draws heavily from the main project, but has some assets removed and other asset and performance changes which support building to Quest. This version lives in a [fork](https://gitlab.com/historyviz/vauquoislite) off of the main project. Contact nickjg98@vt.edu for more information.

3. An 'study' version which has all of assets from the original project but adds support for an experiment being run by Todd and Zach. This version lives in the ``Text_Study`` branch of the main repository. Contact jogle@vt.edu for more information.

The rest of the documentation in this README will refer primarily to the original project as it serves as the main source of truth for Vauquois assets in any of the offshoot projects.

## Folder Structure 
The project is organized as follows:

## Scenes
There are these scenes:

## Systems

### Calibration

### Asset Fading

### Player Detection

### Redirection

## Misc. Information, Notes, & Future Work

# Project Management

// TODO: Info about Asana

# License and Attribution

// TODO: Add MIT license?
// TODO: Todd fill this in